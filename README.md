# Prometheus Trådfri Exporter

Exposes metrics of IKEA Trådfri gateway in prometheus metrics format.

## Configuration

Configuration is done with a simple, small `yaml` file.
You can follow the example in `config.default.yml`.

## Usage

    -config-file string
      	Path to config file
    -get string
      	Perform RAW request and print result.
    -persistence-file string
      	Path to persistence file, where automatically obtained information is persisted (default "persist.yml")
    -port int
      	Port, the HTTP service is listen on. (default 9004)

