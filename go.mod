module gitlab.com/JulianJacobi/prometheus_tradfri_exporter

go 1.14

require (
	github.com/pion/dtls/v2 v2.0.3
	github.com/plgd-dev/go-coap/v2 v2.0.4
	github.com/prometheus/client_golang v1.7.1
	gopkg.in/yaml.v2 v2.3.0
)
