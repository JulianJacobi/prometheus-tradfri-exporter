#!/bin/sh

set -e -x

LINKS=""

while read LINE; do
    NAME=$(echo $LINE | cut -d ' ' -f1)
    URL=$(echo $LINE | cut -d ' ' -f2)
    LINKS="$LINKS --assets-link {\"name\":\"$NAME\",\"url\":\"$URL\"}"
done < uploaded.txt

release-cli create \
    --name "Release v${CI_COMMIT_REF_NAME}" \
    --tag-name ${CI_COMMIT_TAG} \
    --ref ${CI_COMMIT_SHA} \
    --description "$(cat release-notes.md)" \
    ${LINKS}
    

