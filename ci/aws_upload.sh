#!/bin/bash

set -e

if [ -z "$1" ]; then
    echo "Usage:"
    echo "  aws_upload.sh VERSION"
fi

VERSION=$1
S3_BUCKET=prometheus-tradfri-exporter
S3_BASE_PATH=/releases/v${VERSION}/

for FILE in *.tar.gz; do
    aws s3 cp --acl public-read --only-show-errors ${FILE} s3://${S3_BUCKET}${S3_BASE_PATH}${FILE}
    echo "${FILE} https://${S3_BUCKET}.s3.amazonaws.com${S3_BASE_PATH}${FILE}"
done
