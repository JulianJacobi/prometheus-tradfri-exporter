#!/bin/bash

set -e

if [ -z "$1" ]; then
    echo "Usage:"
    echo "  build_all.sh VERSION"
fi

VERSION=$1

echo "Building version ${VERSION}"
echo ""

function build {
    OS=$1
    ARCH=$2

    echo "Building ${OS}-${ARCH}"

    BIN_NAME=prometheus-tradfri-exporter
    OUT_NAME=prometheus-tradfri-exporter_${OS}-${ARCH}_v${VERSION}
    
    GOOS=${OS} GOARCH=${ARCH} go build -o ${BIN_NAME} .

    tar cfvz ${OUT_NAME}.tar.gz ${BIN_NAME}

    rm -f ${BIN_NAME}
    echo "Success"
}

build darwin amd64
build dragonfly amd64
build freebsd 386
build freebsd amd64
build freebsd arm
build linux 386
build linux amd64
build linux arm
build linux arm64
build linux ppc64
build linux ppc64le
build linux mips
build linux mipsle
build linux mips64
build linux mips64le
build netbsd 386
build netbsd amd64
build netbsd arm
build openbsd 386
build openbsd amd64
build openbsd arm
build plan9 386
build plan9 amd64
build solaris amd64
build windows 386
build windows amd64

