package main

import (
    "flag"
    "fmt"
    "log"
    "strconv"
    "net/http"

    prom "github.com/prometheus/client_golang/prometheus"
    "github.com/prometheus/client_golang/prometheus/promhttp"
)


var (
    // Flags
    confFilePath = flag.String("config-file", "", "Path to config file")
    persistanceFilePath = flag.String(
        "persistence-file", "persist.yml",
        "Path to persistence file, where automatically obtained information is persisted",
    )
    port = flag.Int("port", 9004, "Port, the HTTP service is listen on.")
    get = flag.String("get", "", "Perform RAW request and print result.")

    // Prometheus Metrics
    baseLabels = []string{"device_id", "device_name"}
    deviceInfo = prom.NewGaugeVec(prom.GaugeOpts{
        Name: "tradfri_info",
        Help: "TRÅDFRI device info.",
    }, append(baseLabels, "manufacturer", "product", "sw_version"))
    valueLabels = append(baseLabels, "index")
    dimmer = prom.NewGaugeVec(prom.GaugeOpts{
        Name: "tradfri_dimmer",
        Help: "TRÅDFRI device dimmer value.",
    }, valueLabels)
    status = prom.NewGaugeVec(prom.GaugeOpts{
        Name: "tradfri_status",
        Help: "TRÅDFRI device on/off status. 0 = Off, 1 = On",
    }, valueLabels)
)

// Init function
//
// Register prometheus metrics and parse flags
func init() {
    prom.MustRegister(deviceInfo)
    prom.MustRegister(dimmer)
    prom.MustRegister(status)

    flag.Parse()
}

// Main function
func main() {
    if *confFilePath == "" {
        log.Fatal("Missing option '-config-file'")
    }

    conf, err := ReadConfig(*confFilePath)
    if err != nil {
        log.Fatalf("Error while reading config file: %s", err)
    }

    gtw, err := NewGateway(conf.Host + ":5684", conf.SecurityCode, *persistanceFilePath)
    if err != nil {
        log.Fatalf("Error while connecting Trådfri gateway: ", err)
    }

    if *get != "" {
        msg, err := gtw.Get(*get)
        if err != nil {
            log.Fatal(err)
        }
        body, err := msg.ReadBody()
        if err != nil {
            log.Fatal(err)
        }
        fmt.Printf("%s\n", body)
        return
    }

    http.Handle("/metrics", NewMetricsUpdateHandler(promhttp.Handler(), gtw))
    listenAddr := fmt.Sprintf(":%d", *port)
    log.Printf("Listen on %s", listenAddr)
    log.Fatal(http.ListenAndServe(listenAddr, nil))
}

// MetricsUpdateHandler is kind of a middleware
// to update metrics on every request
type MetricsUpdateHandler struct {
    next http.Handler
    gtw  *Gateway
}

// NewMetricsUpdateHandler returns new
// MetricsUpdateHandler instance
func NewMetricsUpdateHandler(next http.Handler, gtw *Gateway) MetricsUpdateHandler {
    h := MetricsUpdateHandler{
        next: next,
        gtw: gtw,
    }
    return h
}

// ServeHTTP method to match http.Handler
// interface
func (h MetricsUpdateHandler) ServeHTTP(res http.ResponseWriter, req *http.Request) {
    log.Print("Metrics request")

    deviceInfo.Reset()
    dimmer.Reset()
    status.Reset()

    devs, err := h.gtw.GetAllDevices()
    if err != nil {
        log.Printf("Error while getting devices:\n%s", err)
        h.next.ServeHTTP(res, req)
        return
    }

    for _, dev := range devs {
        basicLabels := prom.Labels{"device_id": strconv.Itoa(dev.ID),  "device_name": dev.Name}

        deviceInfoLabels := CopyLabels(basicLabels)
        deviceInfoLabels["manufacturer"] = dev.Info.Manufacturer
        deviceInfoLabels["product"] = dev.Info.Product
        deviceInfoLabels["sw_version"] = dev.Info.Version

        deviceInfo.With(deviceInfoLabels).Set(1)

        valueLabels := CopyLabels(basicLabels)
        if dev.IsBulb() {
            for i, b := range *dev.BulbInfo {
                valueLabels["index"] = strconv.Itoa(i)
                dimmer.With(valueLabels).Set(float64(b.Dimmer))
                status.With(valueLabels).Set(float64(b.Status.Int()))
            }
        }
        if dev.IsPlug() {
            for i, p := range *dev.PlugInfo {
                valueLabels["index"] = strconv.Itoa(i)
                status.With(valueLabels).Set(float64(p.Status.Int()))
            }
        }

    }

    h.next.ServeHTTP(res, req)
}

// CopyLabels copy prom.Labels
// to a new Labels object
func CopyLabels(o prom.Labels) prom.Labels {
    n := make(prom.Labels)

    for k, v := range o {
        n[k] = v
    }

    return n
}

