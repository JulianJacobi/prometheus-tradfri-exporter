package main

import (
    "errors"
    "os"

    "gopkg.in/yaml.v2"
)

// Config represents config file structure
type Config struct {
    Host         string
    SecurityCode string `yaml:"securityCode"`
}

// ReadConfig reads config from disk
func ReadConfig(confFilePath string) (*Config, error) {
    confFile, err := os.Open(confFilePath)

    if err != nil {
        return nil, err
    }

    confData := make([]byte, 1024)

    confDataCount, err := confFile.Read(confData)
    if err != nil {
        return nil, err
    }

    conf := Config{}

    err = yaml.Unmarshal(confData[:confDataCount], &conf)

    if err != nil {
        return nil, err
    }

    if conf.Host == "" {
        return nil, errors.New("Missing config option: 'host'")
    }
    if conf.SecurityCode == "" {
        return nil, errors.New("Missing config option: 'securityCode'")
    }

    return &conf, nil
}
