package main

import (
    "bytes"
    "context"
    "errors"
    "fmt"
    "os"
    "strings"
    "time"

    "encoding/json"
    "math/rand"

    "gopkg.in/yaml.v2"

    piondtls "github.com/pion/dtls/v2"
             "github.com/plgd-dev/go-coap/v2/dtls"
             "github.com/plgd-dev/go-coap/v2/message"
             "github.com/plgd-dev/go-coap/v2/message/codes"
             "github.com/plgd-dev/go-coap/v2/udp/client"
             "github.com/plgd-dev/go-coap/v2/udp/message/pool"
)

// Gateway represents a trådfri gateway
type Gateway struct {
    Address         string
    SecurityCode    string
    Credentials     Credentials
    persistencePath string
}

// PersistenceFile represents the structure
// of the credentials persistence file
type PersistanceFile struct {
    Credentials Credentials
}

// Credentials are credentials to authenticate at the gateway
type Credentials struct {
    Identity string
    PSK string
}

// AuthRequest is the structure of the auth request
type AuthRequest struct {
    Username string `json:"9090"`
}

// AuthResponse is the structure of the auth response
type AuthResponse struct {
    PSK     string `json:"9091"`
    Version string `json:"9029"`
}

// NewGateway creates a new gateway instance
func NewGateway(address, psk, persistencePath string) (*Gateway, error) {
    g := Gateway{
        Address: address,
        SecurityCode: psk,
        Credentials: Credentials{
            Identity: "Client_identity",
            PSK:      psk,
        },
        persistencePath: persistencePath,
    }

    g.readPersistence()

    err := g.testConnection()
    if err != nil {
        err = g.getNewCredentials()
        if err != nil {
            return nil, err
        }
        err := g.testConnection()
        if err != nil {
            return nil, err
        }
    }

    return &g, nil
}

// getNewGatewayCredentials aquire new credentials from gateway
func (g *Gateway) getNewCredentials() error {
    g.Credentials.Identity = "Client_identity"
    g.Credentials.PSK = g.SecurityCode
    seededRand := rand.New(rand.NewSource(time.Now().UnixNano()))
    charset := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
    user_suffix := make([]byte, 5)
    for i := range user_suffix {
        user_suffix[i] = charset[seededRand.Intn(len(charset))]
    }
    req := AuthRequest{
        Username: "Prometheus_exporter_" + string(user_suffix),
    }

    msg, err := g.Post("/15011/9063", req)
    if err != nil {
        return err
    }

    resp := AuthResponse{}
    rData, err := msg.ReadBody()
    if err != nil {
        return err
    }

    err = json.Unmarshal(rData, &resp)
    if err != nil {
        return err
    }

    g.Credentials.Identity = req.Username
    g.Credentials.PSK = resp.PSK

    return g.writePersistence()
}

// testConnection tests if the authenticated user is still valid
func (g *Gateway) testConnection() error {
    msg, err := g.Get("/15001")
    if err != nil {
        return err
    }
    if msg.Code() == codes.Unauthorized {
        return errors.New("Unauthorized")
    }
    return nil
}

// Get sends GET request to server
func (g *Gateway) Get(path string) (*pool.Message, error) {
    con, err := g.getConnection()
    if err != nil {
        return nil, err
    }
    defer con.Close()
    return g.GetWithConn(path, con)
}

// GetWithCon is the same as Get but with an existing connection
func (g *Gateway) GetWithConn(path string, con *client.ClientConn) (*pool.Message, error) {
    ctx, cancel := g.getContext()
    defer cancel()
    return con.Get(ctx, path)
}

// Post sends POST request to server
//
// payload must be JSON marshalable
func (g *Gateway) Post(path string, payload interface{}) (*pool.Message, error) {
    con, err := g.getConnection()
    if err != nil {
        return nil, err
    }
    defer con.Close()
    return g.PostWithConn(path, payload, con)
}

// PostWithConn is the same as Post but with an existing connection
func (g *Gateway) PostWithConn(path string, payload interface{}, con *client.ClientConn) (*pool.Message, error) {
    data, err := json.Marshal(payload)
    if err != nil {
        return nil, err
    }
    ctx, cancel := g.getContext()
    defer cancel()
    return con.Post(ctx, path, message.AppJSON, bytes.NewReader(data))
}

// getConnection opens a connection to the gateway
func (g *Gateway) getConnection() (*client.ClientConn, error) {
    return dtls.Dial(g.Address, &piondtls.Config{
        PSK: func(hint []byte) ([]byte, error) {
			return []byte(g.Credentials.PSK), nil
		},
        PSKIdentityHint: []byte(g.Credentials.Identity),
        CipherSuites:    []piondtls.CipherSuiteID{piondtls.TLS_PSK_WITH_AES_128_CCM_8},
    })
}

// getContext returns the default context for requests
func (g *Gateway) getContext() (context.Context, context.CancelFunc) {
    return context.WithTimeout(context.Background(), time.Second)
}

// readPersistence reads persisted credentials from persistence file
func (g *Gateway) readPersistence() error {
    file, err := os.Open(g.persistencePath)
    if err != nil {
        return err
    }

    info, err := file.Stat()
    if err != nil {
        return err
    }

    data := make([]byte, info.Size())

    count, err := file.Read(data)
    if err != nil {
        return err
    }

    d := PersistanceFile{}

    err = yaml.Unmarshal(data[:count], &d)

    if err != nil {
        return err
    }

    if d.Credentials.Identity != "" && d.Credentials.PSK != "" {
        g.Credentials = d.Credentials
    }
    return nil
}

// writePersistence writes credentials to file for persistence
func (g *Gateway) writePersistence() error {
    file, err := os.Create(g.persistencePath)
    if err != nil {
        return err
    }

    data, err := yaml.Marshal(&PersistanceFile{
        Credentials: g.Credentials,
    })
    if err != nil {
        return err
    }

    _, err = file.Write(data)
    return err
}

// GetDevice returns a device from gateway
func (g *Gateway) GetDevice(id int) (*Device, error) {
    con, err := g.getConnection()
    if err != nil {
        return nil, err
    }
    defer con.Close()
    return g.GetDeviceWithConn(id, con)
}

// GetDeviceWithConn is the same as GetDevice but with existing connection
func (g *Gateway) GetDeviceWithConn(id int, con *client.ClientConn) (*Device, error) {
    msg, err := g.GetWithConn(fmt.Sprintf("/15001/%d", id), con)
    if err != nil {
        return nil, err
    }

    var dev Device

    body, err := msg.ReadBody()
    if err != nil {
        return nil, err
    }
    err = json.Unmarshal(body, &dev)
    dev.Raw = body
    return &dev, err
}

// GetAllDevices returns a list of all devices
// registered to the gateway
func (g *Gateway) GetAllDevices() ([]*Device, error) {
    con, err := g.getConnection()
    if err != nil {
        return []*Device{}, err
    }
    defer con.Close()
    listMsg, err := g.GetWithConn("/15001", con)
    if err != nil {
        return []*Device{}, err
    }

    listBody, err := listMsg.ReadBody()
    if err != nil {
        return []*Device{}, err
    }

    var list []int
    err = json.Unmarshal(listBody, &list)

    var devs []*Device

    for _, id := range list {
        dev, err := g.GetDeviceWithConn(id, con)
        if err != nil {
            return devs, err
        }
        devs = append(devs, dev)
    }

    return devs, nil
}

// Device represents a device, connected to the gateway
type Device struct {
    Name         string        `json:"9001"`
    ID           int           `json:"9003"`
    BuildDate    int           `json:"9002"`
    LastSeen     int           `json:"9020"`
    Unknown_5750 int           `json:"5750"`
    Unknown_9054 int           `json:"9054"`
    Unknown_9019 int           `json:"9019"`
    Info         DeviceInfo    `json:"3"`
    BulbInfo     *[]BulbInfo   `json:"3311"`
    PlugInfo     *[]PlugInfo   `json:"3312"`
    RemoteInfo   *[]RemoteInfo `json:"15009"`

    Raw          []byte
}

// IsBulb is true if the device is a bulb
func (d *Device) IsBulb() bool {
    return d.BulbInfo != nil
}

// IsPlug is true if the device is a plug
func (d *Device) IsPlug() bool {
    return d.PlugInfo != nil
}

// IsRemote is true if the device is a remote
func (d *Device) IsRemote() bool {
    return d.RemoteInfo != nil
}

// String returns the String representation of the device
func (d *Device) String() string {
    ts := make([]string, 0)

    var status string

    if d.IsBulb() {
        ts = append(ts, "Bulb")
        for i, bulb := range *d.BulbInfo {
            status = fmt.Sprintf(
                "%s\n  Bulb #%d:\n    Status: %s",
                status, i, bulb.Status,
            )
        }
    }
    if d.IsPlug() {
        ts = append(ts, "Plug")
    }
    if d.IsRemote() {
        ts = append(ts, "Remote")
    }

    return fmt.Sprintf(
        "Type: %s\n  Name: %s%s\n%v",
        strings.Join(ts, ", "), d.Name, status, string(d.Raw),
    )
}

// DeviceInfo are the basid device infos
type DeviceInfo struct {
    Manufacturer string `json:"0"`
    Product      string `json:"1"`
    Unknown_2    string `json:"2"`
    Version      string `json:"3"`
    Unknown_6    int    `json:"6"`
}

// BulbInfo are bulb specific infos
type BulbInfo struct {
    Status           Status `json:"5850"`
    Dimmer           uint8  `json:"5851"`
    ColorTemperature int    `json:"5711"`
    ColorX           int    `json:"5709"`
    ColorY           int    `json:"5710"`
}

// RemoteInfo are remote specific infos
type RemoteInfo struct {}

// PlugInfo are plug specific infos
type PlugInfo struct {
    Status Status `json:"5850"`
    Dimmer uint8  `json:"5851"`
}

// Status is an on/off status
type Status struct {
    Value bool
}

// Int represents the status as int
//
//  on = 1
//  off = 0
func (s Status) Int() int {
    if s.Value {
        return 1
    } else {
        return 0
    }
}

// String return the status as string
func (s Status) String() string {
    if s.Value {
        return "On"
    } else {
        return "Off"
    }
}

// UnmarshalJSON is a custom JSON unmarshaler for status
func (s *Status) UnmarshalJSON(data []byte) error {
    var v int

    err := json.Unmarshal(data, &v)
    if err != nil {
        return err
    }

    s.Value = (v > 0)

    return nil
}

