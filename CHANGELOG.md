# CHANGELOG

## v0.1.1

 * suffix credentials username with random string
   to avoid errors with same username in multiple clients

## v0.1

 * first working version

